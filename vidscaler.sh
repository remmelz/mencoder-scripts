#!/bin/bash

###########################################
# Default variables
###########################################

video=""
thumb=15
bitrate=96
scale=720
delete="False"
encoder="ffmpeg"

###########################################
# Arguments
###########################################

optstring="di:t:s:b:e:"

while getopts ${optstring} arg; do
  case "${arg}" in

    d) delete="True"     ;;
    i) video="${OPTARG}" ;;
    t) thumb="${OPTARG}" ;;
    b) bitrate=${OPTARG} ;;
    s) scale=${OPTARG}   ;;
    e) encoder=${OPTARG} ;;

    ?)
      echo "Invalid option: -${OPTARG}."
      echo
      usage
      ;;
  esac
done

###########################################
# Checking arguments
###########################################

_err=0
which ffmpeg   > /dev/null || _err=1
which mencoder > /dev/null || _err=1

if [[ ${_err} -gt 0 ]]; then
  echo 'Some required programs are missing.'
  exit 1
fi

if [[ -f $1 ]]; then
  video=$1
fi

if [[ ! -f ${video} ]]; then
  echo "Error: no input file"
  exit 1
fi

###########################################
# Start scaling
###########################################

video_small=`echo ${video} \
  | cut -d'.' -f1`"_s.mp4"

if [[ -f ${video_small} ]]; then
  echo "Skipping: Already converted."
  exit 1
fi

###########################################
# Mencoder Two pass
###########################################

if [[ ${encoder} == 'mencoder' ]]; then

  mencoder ${video} \
    -oac twolame -twolameopts br=${bitrate} \
    -ovc x264 -x264encopts pass=1:bitrate=${bitrate} \
    -vf scale=${scale}:-3 \
    -o /dev/null
  
  mencoder ${video} \
    -oac twolame -twolameopts br=${bitrate} \
    -ovc x264 -x264encopts pass=2:bitrate=${bitrate} \
    -vf scale=${scale}:-3 \
    -o _tmp.mp4
fi

###########################################
# FFMPEG (Quick) Single pass
###########################################

if [[ ${encoder} == 'ffmpeg' ]]; then

   ffmpeg -i ${video} \
   -preset slower \
   -crf 18 \
   -qscale:a 10 \
   -c:v libx264 \
   -c:a libmp3lame -b:a 128k \
   -vf scale=480:-1 \
   -y _tmp.mp4

fi

# Extract a video picture
ffmpeg -y -i _tmp.mp4 \
  -frames:v 1 -ss ${thumb} thumb.png

# Use as video thumbnail
ffmpeg -y -fflags \
  +genpts -i _tmp.mp4 -i thumb.png \
  -map 1 -map 0 -c copy \
  -disposition:0 attached_pic \
  ${video_small}

if [[ -n `echo ${delete} | grep -i 'true'` ]]; then
  rm ${video}
fi

###########################################
# Cleanup tempfiles
###########################################

rm -f _tmp.mp4 thumb.png divx2pass*

exit 0
